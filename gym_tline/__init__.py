import logging
from gym.envs.registration import register

logger = logging.getLogger(__name__)

register(
    id='Tline-v0',
    entry_point='gym_tline.envs:TlineEnv',
)
