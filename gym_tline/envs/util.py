import numpy as np


def check_current(currents, current, max_current, margin=5e-3):
    """
    Checks if the current is in currents within a
    relative margin of margin
    returns if existing and index
    """

    if len(currents) == 0:
        return False, np.NaN

    idx = (np.abs(currents - np.abs(current))).argmin()

    if np.abs((currents[idx] - np.abs(current)) / max_current) < margin:
        return True, idx
    elif np.abs(current / max_current) < margin:
        return True, -1
    else:
        return False, np.NaN


def perfect_fodo_start_twiss(mu, l_cell, klmax=np.inf, fodo_entrance_foc=True):
    """  returns the beam parameters at the start of a bendless FODO"""
    kl = 4 * np.sin(mu / 2) / l_cell
    if np.abs(kl) > klmax * 0.8:
        kl = klmax * np.random.uniform(0.2, 0.8, 1).item()
        mu = 2 * np.arcsin(kl * l_cell / 4)

    if (np.abs(kl) + 0.2 * klmax) > 4 / l_cell:
        kl = (4 / l_cell - 0.1 * klmax) * np.sign(kl)
        mu = 2 * np.arcsin(kl * l_cell / 4)

    beta1 = np.abs(l_cell / np.sin(mu) * (1 + np.sin(mu / 2)))
    alpha1 = (-1 - np.sin(mu / 2)) / np.cos(mu / 2)

    beta2 = np.abs(l_cell / np.sin(mu) * (1 - np.sin(mu / 2)))
    alpha2 = (+1 - np.sin(mu / 2)) / np.cos(mu / 2)

    if fodo_entrance_foc:
        return alpha1, beta1, alpha2, beta2, kl, mu
    else:
        return alpha2, beta2, alpha1, beta1, kl, mu


def norm_parm(xmin, xmax, x, clip=False, clipmin=None, clipmax=None):
    x = (x - xmin) / (xmax - xmin) * 2 - 1

    if clip:
        x = np.clip(x, clipmin, clipmax)

    return x
