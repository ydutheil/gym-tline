import gym
import matplotlib as mpl
import matplotlib.pyplot as plt
import numpy as np
from cpymad import libmadx
from cpymad.madx import Madx
from gym import spaces

from .util import (
    check_current, perfect_fodo_start_twiss, norm_parm)

MAX_MADX_LATS_COUNTER = 1000


class TlineEnv(gym.Env):
    metadata = {'render.modes': ['human']}

    def __init__(self,
                 lowlevel_madx=False, suppress_madx_output=True,
                 quads=None, bends=None,
                 max_drift_length=150,
                 verbose=False,
                 range_fodo_pc=(5, 5),  # GeV
                 range_fodo_length=(15, 15),  # m
                 range_fodo_phase_advance=np.deg2rad([60, 60]),
                 range_fodo_entrance_foc=[True, False],
                 range_fodo_exit_middle=[True, False],

                 range_pos_x=(0, 0),  # m for left/right position
                 range_pos_y=(0, 0),  # m for vertical up/down position
                 range_pos_z=(50, 150),  # m for longitudinal position

                 range_ang_theta=(0, 0),  # rad for bend around Y
                 range_ang_phi=(0, 0),  # rad for vertical slope
                 range_ang_psi=(0, 0),  # rad for roll angle

                 norm_range_beta=(0, 1000),
                 norm_range_alpha=(-10, 10),
                 norm_range_s=(0, 200),
                 norm_range_rigidity=(0, 35),
                 norm_range_reward=(-10, 20),
                 max_number_currents=8,
                 weight_number_currents_used=0.5
                 ):

        self.range_fodo_exit_middle = range_fodo_exit_middle
        self.range_fodo_entrance_foc = range_fodo_entrance_foc
        self.weight_number_currents_used = weight_number_currents_used
        self.max_number_currents = max_number_currents
        self.norm_range_reward = norm_range_reward
        self.norm_range_rigidity = norm_range_rigidity
        self.norm_range_s = norm_range_s
        self.norm_range_alpha = norm_range_alpha
        self.norm_range_beta = norm_range_beta

        self.range_fodo_pc = range_fodo_pc
        self.range_fodo_length = range_fodo_length
        self.range_fodo_phase_advance = range_fodo_phase_advance
        self.range_pos_x = range_pos_x
        self.range_pos_y = range_pos_y
        self.range_pos_z = range_pos_z
        self.range_ang_theta = range_ang_theta
        self.range_ang_psi = range_ang_psi
        self.range_ang_phi = range_ang_phi
        if bends is None:
            bends = dict()
        if quads is None:
            quads = dict()
        self.verbose = verbose
        self.length = np.sqrt(range_pos_x[1] ** 2 + range_pos_y[1] ** 2 + range_pos_z[1] ** 2) * 1.5
        self.max_drift_length = max_drift_length

        self.quads = quads
        self.bends = bends

        self._init_action_space()
        self._init_observation_space()

        self.lowlevel_madx = lowlevel_madx
        self.suppress_madx_output = suppress_madx_output

        self._init_madx_engine()

        _ = self.reset()

    def _init_madx_engine(self, restart=False):

        if restart and self.lowlevel_madx:
            self.madx.quit()
            libmadx.finish()
        elif restart:
            self.madx.quit()

        if self.lowlevel_madx:
            self.madx = Madx(libmadx)
        elif self.verbose:
            self.madx = Madx(command_log='madx_logger')
        else:
            self.madx = Madx()

        if self.suppress_madx_output:
            self.madx.option(echo=False, warn=False, info=False, twiss_print=False)

        self.madx_lats_counter = 0

    def _init_observation_space(self):
        self.observation_space = spaces.Box(

            low=np.array([
                # twiss final
                -1, -8, -1, -8,  # betx, alfx, bety, alfy

                # twiss at current location
                -1, -8, -1, -8,  # betx, alfx, bety, alfy

                # used currents
                *np.ones(self.max_number_currents) * -1.01,  # max 5 currents

                # s distance to end
                -1.1,

                # beam rigidity
                -1.1
            ]),

            high=np.array([
                # twiss final
                100, 8, 100, 8,  # betx, alfx, bety, alfy

                # twiss at current location
                100, 8, 100, 8,  # betx, alfx, bety, alfy

                # used currents
                *np.ones(self.max_number_currents) * 1.01,  # max 5 currents

                # s distance to end
                5,

                # beam rigidity
                2
            ]),
            dtype=np.float32)

    def _init_action_space(self):
        # single dimension, quad strength in relative
        self.action_space = spaces.Box(
            low=np.array([-1]),
            high=np.array([+1]),
            dtype=np.float32)

    def _insert_element(self, elem, fractional_strength, base_type):

        elem_idx = 0
        while ((elem['name'] + '_{:d}'.format(elem_idx))
               in self.madx.sequence['s1'].elements):
            elem_idx += 1

        if base_type == 'quadrupole':
            self.madx.command.quadrupole.clone(elem['name'] + '_{:d}'.format(elem_idx),
                                               l=elem['length'],
                                               k1=elem['max int. G'] / self.beam_rigidity / elem[
                                                   'length'] * fractional_strength)
        else:
            raise NotImplementedError('no element type', base_type)

        self.madx.command.seqedit(sequence='s1')
        self.madx.command.install(element=elem['name'] + '_{:d}'.format(elem_idx),
                                  at=self.s + elem['length'] / 2)
        self.madx.command.endedit()

    def _move_end_marker(self):
        self.madx.command.seqedit(sequence='s1')
        self.madx.command.move(element='endmarker', to=self.s)
        self.madx.command.endedit()

    def _step_drift(self, drift_length):
        self.s += np.abs(drift_length)

    def _step_quad(self, action):
        """introduce a quadrupole and increments the s location """
        quad = self.quads[0]
        fractional_strength = action[0]
        self._insert_element(quad, fractional_strength, 'quadrupole')

        self.s += quad['length']

        existing_strength, index = check_current(self.currents, fractional_strength,
                                                 1)
        if not existing_strength:
            self.currents = np.append(self.currents, np.abs(fractional_strength))
            additional_cost = 3 * quad['base cost']
        else:
            additional_cost = 0

        self.total_cost += quad['base cost'] + additional_cost

    def step(self, action: list, w_eval=True):
        """select quadrupole strength and place a drift
        """
        if self.verbose:
            print('got action ', action)
        if np.isnan(action).any():
            print('**********************, got action ', action)
            raise ValueError('got nan action')

        self._step_quad(action)

        self._step_drift(self.fodo_length / 2 - self.quads[0]['length'])

        self._move_end_marker()

        return self.eval()

    def eval(self, interp=False):
        """
        run twiss & survey and store outputs
        The agent takes a step in the environment.

        Parameters
        ----------

        Returns
        -------
        ob, reward, episode_over, info : tuple
            ob (object) :
                an environment-specific object representing your observation of
                the environment.
            reward (float) :
                amount of reward achieved by the previous action. The scale
                varies between environments, but the goal is always to increase
                your total reward.
            episode_over (bool) :
                whether it's time to reset the environment again. Most (but not
                all) tasks are divided up into well-defined episodes, and done
                being True indicates the episode has terminated. (For example,
                perhaps the pole tipped too far, or you lost your last life.)
            info (dict) :
                 diagnostic information useful for debugging. It can sometimes
                 be useful for learning (for example, it might contain the raw
                 probabilities behind the environment's last state change).
                 However, official evaluations of your agent are not allowed to
                 use this for learning.
        """
        info = {}

        self.madx.use(sequence='s1', range='#s/endmarker')

        if interp:
            self.madx.select(flag='interpolate', step=0.05)
        else:
            self.madx.select(flag='interpolate', clear=True)

        self.twissall = self.madx.twiss(  # TWISS
            betx=self.twiss0['betx'], alfx=self.twiss0['alfx'],
            bety=self.twiss0['bety'], alfy=self.twiss0['alfy'],
            dx=self.twiss0['dx'], dpx=self.twiss0['dpx'],
            dy=self.twiss0['dy'], dpy=self.twiss0['dpy']).dframe()

        self.twisss = dict(self.twissall.iloc[-1]
                           [['betx', 'alfx', 'bety', 'alfy',
                             'dx', 'dpx', 'dy', 'dpy', 's']]
                           )

        self.xall = self.madx.survey(  # SURVEY
            x0=self.x0['x'], y0=self.x0['y'], z0=self.x0['z'],
            theta0=self.x0['theta'], phi0=self.x0['phi'], psi0=self.x0['psi']).dframe()
        self.xs = dict(self.xall.iloc[-1]
                       [['x', 'y', 'z', 'theta', 'phi', 'psi']])

        self.observation_dict = {
            'twiss final': np.array(
                [self.twissf[key] for key in
                 ['betx', 'alfx', 'bety', 'alfy']],
                dtype=np.float32),
            'twiss current': np.array(
                [self.twisss[key] for key in
                 ['betx', 'alfx', 'bety', 'alfy']],
                dtype=np.float32),
            'used current': np.array(
                np.append(
                    self.currents, np.zeros(self.max_number_currents - len(self.currents))),
                # change hard-coded number of currents
                dtype=np.float32),
            's distance to end': np.array(
                [self.line_length - self.twisss['s']],  # change hard-coded number of currents
                dtype=np.float32),
            'beam rigidity': np.array(
                [self.beam_rigidity],
                dtype=np.float32),
        }
        self.observation = np.hstack([
            self.observation_dict[key] for key in
            ['twiss final', 'twiss current', 'used current',
             's distance to end', 'beam rigidity']])

        # observation normalization
        self.observation_normed = np.array([
            # twiss final  betx, alfx, bety, alfy
            norm_parm(*self.norm_range_beta, self.observation[0]),
            norm_parm(*self.norm_range_alpha, self.observation[1]),
            norm_parm(*self.norm_range_beta, self.observation[2]),
            norm_parm(*self.norm_range_alpha, self.observation[3]),
            # twiss at current location # betx, alfx, bety, alfy
            norm_parm(*self.norm_range_beta, self.observation[4]),
            norm_parm(*self.norm_range_alpha, self.observation[5]),
            norm_parm(*self.norm_range_beta, self.observation[6]),
            norm_parm(*self.norm_range_alpha, self.observation[7]),
            # used currents
            *self.observation_dict['used current'],
            # s distance to end
            norm_parm(*self.norm_range_s, *self.observation_dict['s distance to end']),
            # beam rigidity
            norm_parm(*self.norm_range_rigidity, *self.observation_dict['beam rigidity'])
        ])

        # reward building and episode termination

        # episode fails if any of the state exceeds the bound
        if (self.observation_normed > self.observation_space.high).any() or \
                (self.observation_normed < self.observation_space.low).any():
            self.reward = norm_parm(*self.norm_range_reward, -10)
            done = True
            info['reward path'] = 'bounds exceeded'

        elif len(self.currents) == self.max_number_currents:
            self.reward = norm_parm(*self.norm_range_reward, -8)
            done = True
            info['reward path'] = 'max number of currents'

        # arrived at the end of the line
        elif self.observation_dict['s distance to end'] < self.fodo_length / 5:
            goal_reached = 0
            goal_reached += 1 - norm_parm(0, np.sqrt(900),
                                          np.sqrt(np.abs(self.twissf['betx'] - self.twisss['betx'])),
                                          True, -1, 1.5)
            goal_reached += 1 - norm_parm(0, np.sqrt(900),
                                          np.sqrt(np.abs(self.twissf['bety'] - self.twisss['bety'])),
                                          True, -1, 1.5)
            goal_reached += 1 - norm_parm(0, np.sqrt(15),
                                          np.sqrt(np.abs(self.twissf['alfx'] - self.twisss['alfx'])),
                                          True, -1, 1.5)
            goal_reached += 1 - norm_parm(0, np.sqrt(15),
                                          np.sqrt(np.abs(self.twissf['alfy'] - self.twisss['alfy'])),
                                          True, -1, 1.5)

            self.reward = norm_parm(*self.norm_range_reward,
                                    goal_reached * 2.5 -
                                    len(self.currents) * self.weight_number_currents_used)
            done = True
            info['reward path'] = 'end of line'

        else:
            self.reward = 0
            done = False
            info['reward path'] = 'continuing'

        if self.verbose:
            print('{:30s} {:8.4f} {:}'.format(
                info['reward path'], self.reward, done))

        return self.observation_normed, self.reward, done, info

    def reset(self):
        if self.verbose:
            print('******** environement reset')

        self.madx_lats_counter += 1
        if self.madx_lats_counter > MAX_MADX_LATS_COUNTER:  # restart MADX engine, due to issues after many lattices
            self._init_madx_engine(restart=True)

        self.s = 0
        self.real_space_distance_to_target_previous = np.inf
        self.previous_reward = -np.inf

        self.twissf = {'betx': np.nan, 'bety': np.nan}
        while np.isnan([self.twissf['betx'], self.twissf['bety']]).any():
            self._generate_episode_parameters_simple_fodo()

        self.madx.beam(particle='proton', pc='{:10.6e}'.format(self.pc))

        self.madx.command.sequence.clone('s1', l='{:10.6e}'.format(self.length * 2))
        self.madx.elements.marker.clone('endmarker', at=0)
        self.madx.command.endsequence()

        self.currents = np.array([], dtype=np.float32)

        self.madx.use(sequence='s1')
        self.beam_rigidity = self.pc * 3.335639951
        self.total_cost = 0

        self.generated_FODO = False

        obs, *_ = self.eval()
        return obs

    def _choose_episode_parameters(self, twiss0, x0, twissf, xf,
                                   pc=1, eps=None):

        if eps is None:
            eps = {'x': 1.e-6, 'y': 1e-6}
        self.twiss0 = twiss0  # transverse beam parms, alf, bet, d &d' in both planes
        self.twissf = twissf
        self.x0 = x0  # position and angles
        self.xf = xf
        self.pc = pc

        self.eps = eps

    def _generate_episode_parameters_simple_fodo(self):
        """generating parameters for a perfect straight FODO"""
        self.generated_FODO = True

        self.pc = np.random.uniform(*self.range_fodo_pc, 1).item()
        self.beam_rigidity = self.pc * 3.3356410  # Warning, only good for positive 1 charge

        self.fodo_length = np.random.uniform(*self.range_fodo_length, 1).item()
        self.fodo_phase_advance = np.random.uniform(*self.range_fodo_phase_advance, 1).item()

        # select a cell number of cells that fit in the total length
        end_distance_max = np.sqrt(self.range_pos_x[1] ** 2 +
                                   self.range_pos_y[1] ** 2 +
                                   self.range_pos_z[1] ** 2)
        self.n_cells = int(self.range_pos_z[1]/self.fodo_length)
        # while ((self.n_cells + 1.5) * self.fodo_length) > end_distance_max:
        #     self.n_cells = np.random.randint(1, 20)

        # select a quad family
        self.fodo_quad_id = np.random.randint(len(self.quads))

        self.pos_x = 0
        self.pos_y = 0

        # select end at the middle or end of cell
        self.fodo_entrance_foc = np.random.choice(self.range_fodo_entrance_foc)
        self.fodo_exit_middle = np.random.choice(self.range_fodo_exit_middle)
        self.line_length = (self.n_cells - 0.5) * self.fodo_length if self.fodo_exit_middle \
            else self.n_cells * self.fodo_length

        self.ang_theta = 0
        self.ang_phi = 0
        self.ang_psi = 0

        self.x0 = {'x': 0.0, 'y': 0.0, 'z': 0.0, 'theta': 0.0, 'phi': 0.0, 'psi': 0.0}
        self.xf = {'x': 0.0, 'y': 0.0, 'z': self.line_length, 'theta': 0.0, 'phi': 0.0, 'psi': 0.0}

        alpha1, beta1, alpha2, beta2, \
            self.fodo_kl, self.fodo_phase_advance = perfect_fodo_start_twiss(
                self.fodo_phase_advance, self.fodo_length,
                klmax=self.quads[self.fodo_quad_id]['max int. G'] / self.beam_rigidity,
                fodo_entrance_foc=self.fodo_entrance_foc)

        self.twiss0 = {
            'betx': beta1, 'alfx': alpha1, 'dx': 0.0, 'dpx': 0.0,
            'bety': beta2, 'alfy': alpha2, 'dy': 0.0, 'dpy': 0.0}

        if self.fodo_exit_middle:
            self.twissf = {
                'betx': beta2, 'alfx': alpha2, 'dx': 0.0, 'dpx': 0.0,
                'bety': beta1, 'alfy': alpha1, 'dy': 0.0, 'dpy': 0.0}
        else:
            self.twissf = {
                'betx': beta1, 'alfx': alpha1, 'dx': 0.0, 'dpx': 0.0,
                'bety': beta2, 'alfy': alpha2, 'dy': 0.0, 'dpy': 0.0}

    def check_seq_length(self):
        if self.madx.sequence.s1.length < (self.s + 15):
            raise ValueError('s too close to sequence length',
                             self.s, self.madx.sequence.s1.length)

    # noinspection PyUnresolvedReferences,PyMethodOverriding
    def render(self, fig, mode='human', clear_fig=True):
        fig.clear()

        # setting up 4 stacked subplots
        gs = mpl.gridspec.GridSpec(7, 1, height_ratios=[2, 8, 8, 2, 8, 0, 8])
        ax0 = fig.add_subplot(gs[0])
        ax1 = fig.add_subplot(gs[1], sharex=ax0)
        ax2 = fig.add_subplot(gs[2], sharex=ax0)
        ax3 = fig.add_subplot(gs[4])
        ax4 = fig.add_subplot(gs[6], sharex=ax3)

        plt.setp(ax1.get_xticklabels(), visible=False)
        plt.setp(ax3.get_xticklabels(), visible=False)

        # top plot is synoptic
        ax0.axis('off')
        ax0.set_ylim(-1.2, 1)
        ax0.plot([0, self.twissall['s'].max()], [0, 0], 'k-')

        twiss_quads = self.twissall[self.twissall['keyword'] == 'quadrupole']
        for _, row in twiss_quads.iterrows():
            _ = ax0.add_patch(
                mpl.patches.Rectangle(
                    (row['s'] - row['l'], 0), row['l'], np.sign(row['k1l']),
                    facecolor='k', edgecolor='k'))

        twiss_rbends = self.twissall[self.twissall['keyword'] == 'rbend']
        for _, row in twiss_rbends.iterrows():
            _ = ax0.add_patch(
                mpl.patches.Rectangle(
                    (row['s'] - row['l'], -1), row['l'], 2,
                    facecolor='None', edgecolor='k'))

        ax1.set_xlabel('s (m)')
        ax1.set_ylabel(r'$\beta$ (m)')
        ax1.plot(self.twissall.s, self.twissall.betx, 'r-', label='horizontal')
        ax1.plot(self.twissall.s, self.twissall.bety, 'b-', label='vertical')
        ax1.legend()

        ax2.set_xlabel('s (m)')
        ax2.set_ylabel('Dispersion (m)')
        ax2.plot(self.twissall.s, self.twissall.dx, 'r-')

        ax2.plot(self.twissall.s, self.twissall.dy, 'r-')

        ax3.set_xlabel('Z (m)')
        ax3.set_ylabel('X (m), top view')
        ax3.plot(self.xall.z, self.xall.x, 'r-')

        ax4.set_xlabel('Z (m)')
        ax4.set_ylabel('Y (m), side view')
        ax4.plot(self.xall.z, self.xall.y, 'r-')

        fig.tight_layout()
        fig.subplots_adjust(hspace=0)

        ax0.set_xlim(0, self.twissall.s.max())
        ax3.set_xlim(0, self.xall.z.max())

    def close(self):
        self.madx.exit()
