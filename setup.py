#!/usr/bin/env python
# -*- coding: utf-8 -*-

from setuptools import setup, find_packages

setup(name='gym_tline',
      version='0.0.2',
      install_requires=['gym',
                        'cpymad',
                        'matplotlib',
                        'numpy'],  # And any other dependencies foo needs
      packages=find_packages(),
      )
