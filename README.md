
This is a transferline environement. It can be installed using for dev `pip install -e .' or without the **e** for regular install in the python directory.


To use :
```python
import gym
import gym_tline

env = gym.make('Tline-v0', length=1, twiss0=1, x0=1, twissf=1, xf=1)
```


A more comprehensive example is in the `Environment_demo.ipynb` notebook.